# Jupyter Build Example

The purpose of this repo is to experiment with linking libraries to a Jupyter notebook.

## Build Dependencies
- Conan
- CMake

## Notebook with External Libs

### Build all of the libs

If you just want to use an external lib (you're not building and linking your own software), you can simply:

```sh
mkdir build
cd build
conan install ../jupyter_build_example/external_example --build missing
```

At this point, all dependencies will exist in the local Conan cache and all of the pkg_config files are in the `build` folder. So we should be able to set `PKG_CONFIG_PATH` to the `build` folder.

### Idea For Running the Notebook

We need to somehow provide the build output to a notebook server and tell the server where to find the notebook files:

```sh
cd ..
export PKG_CONFIG_PATH=build
notebook_server jupyter_build_example/external_example # what does this look like?
```

Then inside a cling notebook:

```C++
// This sets up EVERYTHING eigen needs and tells cling to link in Eigen
#pragma cling pkg_config(eigen)

#include <iostream>
#include <Eigen/Dense>

Eigen::MatrixXd matrix(2,2);
matrix(0,0) = 3;
matrix(1,0) = 2.5;
matrix(0,1) = -1;
matrix(1,1) = matrix(1,0) + matrix(0,1);
std::cout << matrix << std::endl;
```


## To Build And Use Local and External Libs

### Build all of the libs

If you want to test your own libs, you need to build them and make sure they can also be found via pkg_config.

Assuming you're doing an out-of-source build next to the repo, which is cloned into a folder called `jupyter_build_example`:

```sh
mkdir build
cd build
conan install ../jupyter_build_example/internal_example --build missing
conan build ../jupyter_build_example/internal_example
```

After that, the notebook would be started as above.



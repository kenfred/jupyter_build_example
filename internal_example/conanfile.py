from conans import ConanFile, tools, VisualStudioBuildEnvironment, CMake
from conans.tools import cpu_count, os_info, SystemPackageTool
from conans.util.files import load
from conans.errors import ConanException
import os, sys

class BuildExample(ConanFile):
    settings = "os", "compiler", "build_type", "arch"

    requires = ("eigen/3.3.7@conan/stable", "kfr/3.0.9@bincrafters/stable")
    default_options = {"kfr:header_only": False}

    # Generate cmake info to link during build
    # Generate pkg_config info to link in notebook
    generators = "pkg_config", "cmake"

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()
        # cmake.test()
        # cmake.install()
